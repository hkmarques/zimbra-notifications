/**
 * Module associated with the options page for the extension.
 */
angular.module("zimbraNotificationsOptions", [])
    .controller("OptionsCtrl", function ($scope, $timeout) {

        /**
         * Saves current settings to Chrome's storage.
         */
        $scope.saveOptions = function () {
            var url = $scope.urlZimbra;
            var min = $scope.intervalMinutes;
            chrome.storage.sync.set({
                                        urlZimbra: url,
                                        intervalMinutes: min
                                    }, function() {
                                        $scope.status = "Salvo!"
                                        $scope.$apply();
                                        $timeout(function() {
                                            $scope.status = "";
                                            $scope.$apply();
                                        }, 1000);
                                    });
        };

        /**
         * Restores settings from Chrome's storage.
         */
        $scope.restoreOptions = function () {
            chrome.storage.sync.get({
                                        urlZimbra: "",
                                        intervalMinutes: 10
                                    }, function(items) {
                                        $scope.urlZimbra = items.urlZimbra;
                                        $scope.intervalMinutes = items.intervalMinutes;
                                        $scope.$apply();
                                    });
        };

        /**
         * Initialization code for this controller.
         */
        $scope.init = function () {
            $scope.restoreOptions();
        };

        $scope.init();  // Initializes this controller.

    });