# Zimbra Notifications

------------------

A Chrome plugin for a quick glimpse of your Zimbra Webmail unread messages with notifications.
Zimbra Notifications is an open source extension, you can download all the code from [Bitbucket](https://bitbucket.org/hkmarques/zimbra-notifications), install the extension from the source directory or tweak it to your own taste.
Note that you must be already logged in to your webmail account for the extension to retrieve your unread messages.
The extension is also available from the [Chrome Web Store](https://chrome.google.com/webstore/detail/zimbra-notifications/cfeibfcnlhjphnlphpbkhfhlakgegpoe).
Icons credited to [emey87](http://emey87.deviantart.com/art/Open-envelopes-173698544).

### Permissions required:

 - Access your data on all websites: this is required because your Zimbra Webmail server URL is configured in the options page and cannot be known beforehand, but that is the only external location the extension tries to access.
 - Access your tabs and browsing activity: the extension will open a new tab for your inbox only if there isn't one already open.
 - Storage: for persistent settings.
 - Notifications: that's right, for notifications.
